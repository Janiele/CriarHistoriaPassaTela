package com.example.alunos.livrinho;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity {
    TextView tela3;
    Button btnpassa3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        tela3 = (TextView)findViewById(R.id.textView4);
        btnpassa3 = (Button)findViewById(R.id.button4);

    }
    public void btnpassa3(View v){
        Intent i = new Intent(this, Main4Activity.class);
        startActivity(i);
        finish();
    }
}
