package com.example.alunos.livrinho;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    TextView tela2;
    Button btnpassa2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        tela2 = (TextView)findViewById(R.id.textView2);
        btnpassa2 = (Button)findViewById(R.id.button2);

    }
    public void btnpassa2(View v){
        Intent i =new Intent(this, Main3Activity.class);
        startActivity(i);
        finish();
    }
}
