package com.example.alunos.livrinho;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView tela1;
    Button btnpassa1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tela1 = (TextView)findViewById(R.id.textView);
        btnpassa1 = (Button)findViewById(R.id.button);
    }
    public void btnpassa(View v){
        Intent i =new Intent(this, Main2Activity.class);
        startActivity(i);
        finish();
    }
}
